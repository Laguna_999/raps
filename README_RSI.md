If you came here from the RSI Paper "Analyzing X-Ray tomographies of granular materials":

Due to a small bug in RAPS, the contact number calculated with the fit script was 5.4. This version of RAPS is fixed and will yield the correct value of 5.5.

