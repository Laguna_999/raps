// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.
#ifndef IFILELOADER_H
#define IFILELOADER_H

#include <vector>
#include <string>

#include "cPoint.h"

class IFileLoader
{
    public:
        IFileLoader() {};
        virtual ~IFileLoader() {};

        virtual void LoadPositionsFromFile (std::string strFileName, std::vector<cPoint>& PositionList ) = 0;
        virtual std::string FileTypeName() = 0;
        virtual std::string GetBoxInfoStrongFromFileName (std::string strFileName) =0;
    protected:
    private:
};

#endif // IFILELOADER_H
