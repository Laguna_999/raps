// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.
#ifndef IALGORITHM_H_INCLUDED
#define IALGORITHM_H_INCLUDED

#include <string>
#include <vector>


#include <sstream>
#include <string>

#include <time.h>       /* time_t, struct tm, time, localtime, strftime */

#include <fstream>

#include "cPoint.h"
#include "ISummer.h"


std::string GetCurrentDateTime ()
{
    time_t rawtime;
    struct tm * timeinfo;
    char buffer [80];

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    strftime (buffer,80,"%c",timeinfo);

    return buffer;
}



class IAlgorithm
{
public:
    virtual void Calculate (std::vector<cPoint>& rPoints, ISummer* Histogram ) = 0;

    virtual void Save (std::string parseInfo) = 0;

    std::string GetInfoString ()
    {
        return "# created with RAPS 0.1.2, the professional Program for Random And Packed Structure investigations";
    }

};

#endif // IALGORITHM_H_INCLUDED
