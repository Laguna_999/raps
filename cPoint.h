// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.
#ifndef CPOINT_H
#define CPOINT_H


struct cPoint
{
public:
    cPoint ()
    {
        x = y = z = 0;
        Atheta = Aphi = 0.0;
        Btheta = Bphi = 0.0;
        Ctheta = Cphi = 0.0;
        Alength = Blength = Clength = 0.0;
    };

    double x, y, z;
    /// since we have ellipsoids which might be totally aspherical we have at least two independent axis
    /// these axis are denoted wit A, B and C, where A is the major axis and C is the minor axis
    double Atheta, Aphi ;   // theta in [0,pi/2] phi in [0,2pi]
    double Btheta, Bphi ;   // theta in [0,pi/2] phi in [0,2pi]
    double Ctheta, Cphi ;   // theta in [0,pi/2] phi in [0,2pi]
    double Alength, Blength, Clength;

};

#endif // CPOINT_H
