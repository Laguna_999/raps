// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.
#ifndef CALGOMINMAXPOSITION_H_INCLUDED
#define CALGOMINMAXPOSITION_H_INCLUDED

#include "IAlgorithm.h"

class cAlgoMinMaxPosition :public IAlgorithm
{
public:

// histogram will be ignored for this algo
    void Calculate (std::vector<cPoint>& rPoints, ISummer* Histogram )
    {
        std::cout << "Calculateing Min and Max Positions for The Objects" << std::endl;

        minX = 9999999.9;
        minY = 9999999.9;
        minZ = 9999999.9;

        maxX = - 9999999.9;
        maxY = - 9999999.9;
        maxZ = - 9999999.9;

        for (   std::vector<cPoint>::const_iterator cit = rPoints.begin();
                cit != rPoints.end();
                ++cit)
        {
            Min(minX,cit->x);
            Min(minY,cit->y);
            Min(minZ,cit->z);

            Max(maxX,cit->x);
            Max(maxY,cit->y);
            Max(maxZ,cit->z);
        }

        meanX = (maxX+minX)/2.0;
        meanY = (maxY+minY)/2.0;
        meanZ = (maxZ+minZ)/2.0;


        std::cout << "Min Max Mean:" << std::endl;
        std::cout << "X: " << minX << "\t" << maxX <<  "\t" << meanX << std::endl;
        std::cout << "Y: " << minY << "\t" << maxY <<  "\t" << meanY << std::endl;
        std::cout << "Z: " << minZ << "\t" << maxZ <<  "\t" << meanZ << std::endl;
    };

    void Save (std::string parseInfo)
    {
        std::cout << "Writing MinMax File" << std::endl;
        std::string t_strFileName = "minmax_" + parseInfo +".dat";
        std::ofstream outfile(t_strFileName.c_str());


        outfile << GetInfoString() << std::endl;
        outfile << "# Created on "  << GetCurrentDateTime() << std::endl;
        outfile << "# File for Min Max Positions" << std::endl;
        outfile << "# Min Max Mean:" << std::endl;
        outfile << "#X: " << minX << "\t" << maxX <<  "\t" << meanX << std::endl;
        outfile << "#Y: " << minY << "\t" << maxY <<  "\t" << meanY << std::endl;
        outfile << "#Z: " << minZ << "\t" << maxZ <<  "\t" << meanZ << std::endl;
        outfile << "##" << std::endl;
        outfile << "# minX  minY    minZ    maxX    maxY    maxZ" << std::endl;
        outfile << minX << "\t"<< minY <<"\t"<<  minZ <<"\t"<<  maxX <<"\t"<<  maxY <<"\t"<<  maxZ << std::endl;

        outfile.close();

    };


    double minX;
    double minY;
    double minZ;

    double maxX;
    double maxY;
    double maxZ;

    double meanX;
    double meanY;
    double meanZ;

private:

    void Min (double& minval, double testval)
    {
        if(testval <= minval)
        {
            minval = testval;
        }
    };
    void Max (double& maxval, double testval)
    {
        if(testval >= maxval)
        {
            maxval = testval;
        }
    };
};

#endif // CALGOMINMAXPOSITION_H_INCLUDED
