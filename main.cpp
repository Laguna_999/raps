    // Copyright 2015 Simon Weis

    // This file is part of RAPS.
    //
    // RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
    //
    // RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cmath>

#include <map>
#include <vector>

#include <sstream>
#include <string>

#include <stdlib.h>     /* atof */

#include <time.h>       /* time_t, struct tm, time, localtime, strftime */

#include <fstream>

#include "cSplitString.h"
#include "cSummerLinear.h"

#include "cPoint.h"
#include "cFileLoader_MakseFriction.h"
#include "cFileLoader_MaksePhase.h"
#include "cFileLoader_MPIData.h"
#include "cFileLoader_XYZData.h"

#include "cAlgoMinMaxPosition.h"
#include "cAlgoPairCorrelation.h"
#include "cAlgoAngleDistribution.h"
#include "cAlgoCoordination.h"
#include "cAlgoCNS.h"
#include "cAlgoCNS_Shift.h"


    int main( int argc, char *argv[] )
    {
        std::string t_strBoxGeometryInformation = "n/a";

        std::string datasetpath = "../_data/data.set";

        if(argc == 2)
        {
            datasetpath =  argv[1];
        }


        std::ifstream datafile;
        datafile.open(datasetpath.c_str());
        if(datafile.fail())
        {
            std::cout << "Cannot load data set file "<< std::endl;
            return 0;
        }



        cSplitString strParseInfo("");  // FIXME This line gave me "warning: deprecated conversion from string constant to 'char*' [-Wwrite-strings]"
        while (std::getline(datafile, strParseInfo))
        {


            try
            {
                std::vector<std::string> vecParseInfo = strParseInfo.split(' ');    // this means no space in files or folders

                if(strParseInfo.find("#")!=std::string::npos)
                {
                    continue;
                }

                if(vecParseInfo.size() != 4)
                {
                    throw std::string("number of data strings is not correct. Required Info: [path] [name] [histogrambinwidth] [cutoff (nearest neighbours detection)]");
                }

                std::string filepath = vecParseInfo[0];
                std::string t_strInfostring = vecParseInfo[1];
                std::string t_strHistobinwidth = vecParseInfo[2];
                std::string t_strCutoff = vecParseInfo[3];

                double t_dBinWidth = atof(t_strHistobinwidth.c_str());
                double t_dCutoff = atof(t_strCutoff.c_str());
                double dCutoffRadius = t_dCutoff;

                IFileLoader* FileLoader;
                if (filepath.find("pos.dat")!=std::string::npos)
                {
                    FileLoader = new cFileLoader_MakseFriction();
                }
                else if (filepath.find(".txt")!=std::string::npos)
                {
                    FileLoader = new cFileLoader_MaksePhase();
                }
                else if (filepath.find(".ellip")!=std::string::npos)
                {
                    FileLoader = new cFileLoader_MPIData();
                }
                else if (filepath.find(".dat") != std::string::npos)
                {
                    FileLoader = new cFileLoader_XYZData();
                }
                else if (filepath.find(".xyz") != std::string::npos)
                {
                    FileLoader = new cFileLoader_XYZData();
                }
                else
                {
                    std::cout << "cannot determine file type. Skipping file." << std::endl;
                    continue;
                }

                std::cout << "Working on " << FileLoader->FileTypeName() <<"-File " <<  std::endl << filepath << std::endl;


                std::vector<cPoint> Points; // this is the vector to store the positions in
                FileLoader->LoadPositionsFromFile(filepath, Points);
                delete FileLoader;



                //calculate stuff

                // MinMax (to get an estimated center Position)
                cAlgoMinMaxPosition MinMax;
                {
                    ISummer* pHistoMinMax = new cSummerLinear(1);
                    MinMax.Calculate(Points, pHistoMinMax);
                    MinMax.Save(t_strInfostring);
                    delete pHistoMinMax;
                }

                //Pair Correlation
                {
                    ISummer* pHistoPairCorrelation = new cSummerLinear(t_dBinWidth);
                    cAlgoPairCorrelation PC;
                    PC.Calculate(Points, pHistoPairCorrelation);
                    PC.Save(t_strInfostring);
                    delete pHistoPairCorrelation;
                }
                // Angle Distribution
           {
               ISummer* pHistoAngleDistribution = new cSummerLinear(0.5);  // angle binning 0.5 degree seems legit
               cAlgoAngleDistribution Angle;
               Angle.m_dCutOff = dCutoffRadius;
               Angle.Calculate(Points, pHistoAngleDistribution);
               Angle.Save(t_strInfostring);
               delete pHistoAngleDistribution;
           }
           // Coordination Number
           {
               ISummer* pHistoCoordination = new cSummerLinear(1.0); // you won't get half contacts.
               cAlgoCoordination Coord;
               Coord.m_dCutOff = dCutoffRadius;
               Coord.Calculate(Points, pHistoCoordination);
               Coord.Save(t_strInfostring);
               delete pHistoCoordination;
           }
           // CutOff Coordination Number
           {
                   ISummer* pHistoCoordinationCutoff = new cSummerLinear(1); // binning is done via cutoff radius
                   cAlgoCNS CNS;
                   CNS.m_dCutOff = dCutoffRadius;
                   CNS.Calculate(Points, pHistoCoordinationCutoff);
                   CNS.Save(t_strInfostring);
                   delete pHistoCoordinationCutoff;
           }
           if(false)
           {
                   ISummer* pHistoCoordinationCutoff = new cSummerLinear(1); // binning is done via cutoff radius
                   cAlgoCNS_Shift CNSS;
                   CNSS.dCutoffRadius = dCutoffRadius;
                   CNSS.m_strInfoString = t_strInfostring;
                   CNSS.Calculate(Points, pHistoCoordinationCutoff);
                   delete pHistoCoordinationCutoff;
           }



            std::cout << std::endl << std::endl;
        }
        catch (std::string e)
        {
            std::cout << "ERROR: " << e << std::endl;
            return -1;
        }
    }

    datafile.close();






    return 0;

}
