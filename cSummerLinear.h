// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.
#ifndef CSUMMER_H_INCLUDED
#define CSUMMER_H_INCLUDED

#include<map>
#include <iostream>
#include "ISummer.h"

class cSummerLinear : public ISummer
{
public:
    cSummerLinear (double BinSize): ISummer(BinSize) {};


    void Add(double Radius)
    {
        if(Radius < 0.0)
        {
            throw std::string ("negative Keys are not supported by cSummerLinear.");
        }
        int idx = static_cast<int>(Radius/m_dBinSize);
        //std::cout << idx << " ";
        if ( !m_mapHistogram.count(idx) )
        {
            m_mapHistogram.insert(std::pair<unsigned int,double>(idx,0.0));
        }
        m_mapHistogram.find(idx)->second +=1.0;
    };


    void Add (double Key, double value)
    {
        if(Key < 0.0)
        {
            throw std::string ("negative Keys are not supported by cSummerLinear.");
        }
        unsigned int idx = static_cast<unsigned int>(Key/m_dBinSize);

        if ( !m_mapHistogram.count(idx) )
        {
            m_mapHistogram.insert(std::pair<unsigned int,double>(idx,0.0));
        }
        m_mapHistogram.find(idx)->second += value;

    };

};


#endif // CSUMMER_H_INCLUDED
