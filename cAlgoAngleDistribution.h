// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CALGOANGLEDISTRIBUTION_H_INCLUDED
#define CALGOANGLEDISTRIBUTION_H_INCLUDED


#include "IAlgorithm.h"

class cAlgoAngleDistribution :public IAlgorithm
{
public:
    void Calculate (std::vector<cPoint>& rPoints, ISummer* Histogram )
    {
        m_pHisto = new cSummerLinear(Histogram->GetBinSize());
        m_pHisto->clear();
        double dif1X, dif1Y, dif1Z, dif2X, dif2Y, dif2Z,distanceSquared1, distanceSquared2;
        unsigned int ThreePairs = 0;
        std::cout << "Creating Angle Distribution Histogram with BinSize " << Histogram->GetBinSize() << std::endl;
        for (   std::vector<cPoint>::const_iterator cit1 = rPoints.begin();
                cit1 != rPoints.end();
                ++cit1)
        {
            for (   std::vector<cPoint>::const_iterator cit2 = rPoints.begin();
                    cit2 != rPoints.end();
                    ++cit2)
            {
                if(cit1 == cit2) {continue;}
                dif1X = cit1->x - cit2->x;
                dif1Y = cit1->y - cit2->y;
                dif1Z = cit1->z - cit2->z;
                distanceSquared1 = dif1X*dif1X + dif1Y*dif1Y + dif1Z*dif1Z;
                double distance1 = sqrt(distanceSquared1);
                if (distance1 <= m_dCutOff)
                {
                    for (   std::vector<cPoint>::const_iterator cit3 = rPoints.begin();
                            cit3 != rPoints.end();
                            ++cit3)
                    {
                        if(cit1 == cit3) {continue;}
                        if(cit2 == cit3) {continue;}
                        dif2X = cit1->x - cit3->x;
                        dif2Y = cit1->y - cit3->y;
                        dif2Z = cit1->z - cit3->z;
                        distanceSquared2 = dif2X*dif2X + dif2Y*dif2Y + dif2Z*dif2Z;
                        float distance2 = sqrt(distanceSquared2);
                        if(distance2<= m_dCutOff)
                        {
                            // we have two NN to cit1
                            double cosUnitless = (dif1X*dif2X + dif1Y*dif2Y + dif1Z*dif2Z)/(distance1*distance2);
                            if(cosUnitless >-1 && cosUnitless < 1)
                            {
                                double dAngleInDegree = acos(cosUnitless)*180.0/3.14159;
                                if(dAngleInDegree > 0 && dAngleInDegree < 180)
                                {
                                    m_pHisto->Add(dAngleInDegree);
                                    //std::cout << "detecting angle of: " << dAngleInDegree << std::endl;
                                    ThreePairs++;
                                }
                            }
                        }
                    }
                }
            }
        }


        std::cout << "Three-Pairs found: " << ThreePairs << std::endl;
    };

    ISummer* m_pHisto;

    virtual void Save (std::string parseInfo)
    {
        m_pHisto->clean(); // why so ever there are empty bins in the histogram...

        std::cout << "Writing Angle Distribution" << std::endl;
        std::string t_strFileName_angle = "angle_" + parseInfo +".dat";
        std::ofstream outfile(t_strFileName_angle.c_str());
        outfile << GetInfoString() << std::endl;
        outfile << "# Created on "  << GetCurrentDateTime() << std::endl;
        outfile << "# Histogram of AngleDistribution before an arbitrary cutoff radius" << std::endl;
        outfile << "# CutOff Radius for NearestNeighbour detection "  << m_dCutOff << std::endl;
        outfile << "# Histogram Binsize "  << m_pHisto->GetBinSize() << std::endl;
        //outfile << "# Particles analyzed "  << numberOfParticles << std::endl;
        outfile << "#_1 Angle\t#_2 Occurrence" << std::endl;
        outfile << *m_pHisto << std::endl;
        outfile.close();
        delete m_pHisto;
    };

    double m_dCutOff;

};


#endif // CALGOANGLEDISTRIBUTION_H_INCLUDED
