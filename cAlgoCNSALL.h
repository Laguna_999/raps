// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CALGOCONTACTNUMBERONCUTOFF_H_INCLUDED
#define CALGOCONTACTNUMBERONCUTOFF_H_INCLUDED

#include "IAlgorithm.h"
#include "cSummerLinear.h"

class cAlgoCNS :public IAlgorithm
{
public:
void Calculate (std::vector<cPoint>& rPoints, ISummer* Histogram )
{
        m_dCutOffDelta = m_dCutOff * 0.0004;
        m_dCutOffStart = m_dCutOff - 100 * m_dCutOffDelta;
        m_dCutOffEnd = m_dCutOff + 250 * m_dCutOffDelta;
        m_uiNumberOfPoints = rPoints.size();

        m_pHisto = new cSummerLinear(m_dCutOffDelta);
        for(double dCutOff = m_dCutOffStart; dCutOff<=m_dCutOffEnd; dCutOff+=m_dCutOffDelta)
        {
            //std::cout << "calculating contacts for cutoff " << dCutOff << std::endl;
            ISummer* pHisto = new cSummerLinear(Histogram->GetBinSize());
            float difX, difY, difZ, distanceSquared;
            for (   std::vector<cPoint>::const_iterator cit1 = rPoints.begin();
                    cit1 != rPoints.end();
                    ++cit1)
            {
                unsigned int coordination = 0;
                for (   std::vector<cPoint>::const_iterator cit2 = rPoints.begin();
                        cit2 != rPoints.end();
                        ++cit2)
                {
                    if(cit1 != cit2)
                    {
                        difX = cit1->x - cit2->x;
                        difY = cit1->y - cit2->y;
                        difZ = cit1->z - cit2->z;
                        distanceSquared = difX*difX + difY*difY + difZ*difZ;
                        float distance = sqrt(distanceSquared);
                        if (distance <= dCutOff)
                            coordination++;
                    }
                }
                pHisto->Add(coordination);
            }
            
            
            // calculating mean Contact number
            double dMeanContactNumber = 0.0f;
            std::map<unsigned long, double>::iterator it;
            for(    it = pHisto->GetMap().begin();
                    it != pHisto->GetMap().end();
                    ++it)
            {
                dMeanContactNumber += static_cast<double>(it->first) * it->second;
            }
            dMeanContactNumber /= static_cast<double>( m_uiNumberOfPoints);
            std::cout << "\tmean contact number for cutoff " << dCutOff <<  " is " << dMeanContactNumber << std::endl;
            delete pHisto;
            m_pHisto->Add(dCutOff, dMeanContactNumber);
        }
    };

    // the cutoff as it is expected
    double m_dCutOff;
    unsigned int m_uiNumberOfPoints;

    virtual void Save (std::string parseInfo)
    {
        std::cout << "Writing cutoffContact File" << std::endl;
        std::string t_strFileName = "cns_" + parseInfo +".dat";
        std::ofstream outfile(t_strFileName.c_str());

        outfile << GetInfoString() << std::endl;
        outfile << "# Created on "  << GetCurrentDateTime() << std::endl;
        outfile << "# Histogram cutoff-meancontact number" << std::endl;
        outfile << "# Created on "  << GetCurrentDateTime() << std::endl;

        outfile << "#_1 Cutoff \t#_2Contact Number" << std::endl;
        outfile << *m_pHisto << std::endl;
        outfile.close();
        delete m_pHisto;
    };



private:
    // just for looping
    double m_dCutOffStart;
    double m_dCutOffDelta;
    double m_dCutOffEnd;
    ISummer* m_pHisto;
};

#endif // CALGOCONTACTNUMBERONCUTOFF_H_INCLUDED

