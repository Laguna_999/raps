# RAPS  #
This is RAPS, the program for  Random and Packed Structure Investigations.


## Usage ##

To compile the program, just execute the build.bsh shell script. You will need the latest version of g++, but no other external libraries or programs.
If you want to use codeblocks, there is a codeblocks project file available. With codeblocks RAPS will also compile under windows.

There is an example.dataset file that holds the information about the position files that should be processed. Please change this to your needs. There is also an example position file, particles.xyz included. Please see this file's header for further information.

To execute the program, just call ./RAPS [path to dataset file].

## About ##
Raps was written by Simon Weis between 2013 and 2015. It is released under GNU LGPL, see COPYING and COPYING.LESSER for more details.