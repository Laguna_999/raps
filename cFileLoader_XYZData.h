// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
#ifndef CFILELOADER_XYZDATA_INCLUDED
#define CFILELOADER_XYZDATA_INCLUDED

#include "IFileLoader.h"

class cFileLoader_XYZData : public IFileLoader
{
    public:
    void LoadPositionsFromFile (std::string strFileName, std::vector<cPoint>& PositionList)
    {
        std::ifstream infile;
        infile.open(strFileName.c_str(),std::ifstream::in);
        if (infile.fail())
        {
            std::cout << "Cannot load file " << strFileName << std::endl;
            return;
        }

        std::string line;
        unsigned int linesloaded = 0;
        std::getline(infile, line);
        std::istringstream dimstream(line);
        while (std::getline(infile, line))
        {
            if(line.find("#")!=std::string::npos) continue; // ignore comment lines

            std::istringstream iss(line);
            float x, y, z;
            if (!(iss >> x >> y >> z))
            {
                break;    // error
            }

            cPoint p;
            p.x = x;
            p.y = y;
            p.z = z;
            linesloaded++;
            PositionList.push_back(p);

        }
        std::cout << "Lines loaded: " << linesloaded << std::endl << std::endl;

        infile.close();
    };

    std::string GetBoxInfoStrongFromFileName (std::string strFileName)
    {
        return "No Box Info provided by XYZ-Files";

    };

    std::string FileTypeName()
    {
        return "XYZ_Data";

    };

};



#endif // CFILELOADER_MAKSEFRICTION_INCLUDED
