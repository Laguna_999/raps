#ifndef CALGOCONTACTNUMBERSHIFT_H_GUARD
#define CALGOCONTACTNUMBERSHIFT_H_GUARD

#include <random>
#include "IAlgorithm.h"
#include "cSummerLinear.h"
#include "cAlgoCNS.h"

class cAlgoCNS_Shift : public IAlgorithm
{

    void ShiftPoints(std::vector<cPoint>const  & in, std::vector<cPoint>& out, double sigma)
    {
        out.clear();
        out.resize(in.size());
        std::default_random_engine r;
        std::normal_distribution<double> d(0, sigma);

        for(size_t i = 0; i != in.size(); ++i)
        {
            cPoint q;
            q.x = in[i].x + d(r);
            q.y = in[i].y + d(r);
            q.z = in[i].z + d(r);
            out[i] = q;
        }

    };

public:

    std::string m_strInfoString;
    double dCutoffRadius;
void Calculate (std::vector<cPoint>& rPoints, ISummer* Histogram )
{
    double sigma = 0;
    {
    // calculate original distribution
    ISummer* pHistoCoordinationCutoff = new cSummerLinear(1); // binning is done via cutoff radius
    cAlgoCNS CNS;
    CNS.m_dCutOff = dCutoffRadius;
    CNS.Calculate(rPoints, pHistoCoordinationCutoff);
    std::string info = m_strInfoString + "_sigma" + std::to_string(sigma);
    CNS.Save(info);
    }
    std::vector<double> sigmas = {0.001*dCutoffRadius, 0.005*dCutoffRadius, 0.01*dCutoffRadius, 0.1*dCutoffRadius};
    for(double s :sigmas)
    {
        std::cout << "working for sigma= " << s << std::endl;
        std::vector<cPoint> shifted;
        ShiftPoints(rPoints, shifted, s);

           ISummer* pHistoCoordinationCutoff = new cSummerLinear(1); // binning is done via cutoff radius
        cAlgoCNS CNS;
        CNS.m_dCutOff = dCutoffRadius;
        CNS.Calculate(shifted, pHistoCoordinationCutoff);
        std::string info = m_strInfoString + "_sigma" + std::to_string(s);
        CNS.Save(info);
    }


};

    virtual void Save (std::string parseInfo)
    {
    };
};


#endif
