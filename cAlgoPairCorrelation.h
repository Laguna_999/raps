// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.
#ifndef CALGOPAIRCORRELATION_H_INCLUDED
#define CALGOPAIRCORRELATION_H_INCLUDED

#include "IAlgorithm.h"

class cAlgoPairCorrelation :public IAlgorithm
{
public:
    void Calculate (std::vector<cPoint>& rPoints, ISummer* Histogram )
    {
        m_pHisto = new cSummerLinear(Histogram->GetBinSize());
        std::cout << "Creating Pair Correlation Histogram with BinSize " << Histogram->GetBinSize() << std::endl;
        float difX, difY, difZ, distanceSquared;
        numberOfParticles = rPoints.size();
        numberOfEntries = 0;
        for (   std::vector<cPoint>::const_iterator cit1 = rPoints.begin();
                cit1 != rPoints.end();
                ++cit1)
        {
            for (   std::vector<cPoint>::const_iterator cit2 = rPoints.begin();
                    cit2 != rPoints.end();
                    ++cit2)
            {
                if(cit1 != cit2)
                {
                    difX = cit1->x - cit2->x;
                    difY = cit1->y - cit2->y;
                    difZ = cit1->z - cit2->z;
                    distanceSquared = difX*difX + difY*difY + difZ*difZ;
                    float distance = sqrt(distanceSquared);
                    m_pHisto->Add(distance);
                    numberOfEntries++;
                }
            }
        }
    };
    ISummer* m_pHisto;

    virtual void Save (std::string parseInfo)
    {
        std::cout << "Writing Pair Correlation File" << std::endl;
        std::string t_strFileName = "pc_" + parseInfo +".dat";
        std::ofstream outfile(t_strFileName.c_str());

        outfile << GetInfoString() << std::endl;
        outfile << "# Created on "  << GetCurrentDateTime() << std::endl;
        outfile << "# Pair Correlation file" << std::endl;
        outfile << "# Histogram Binsize "  << m_pHisto->GetBinSize() << std::endl;
        outfile << "# Particles analyzed "  << numberOfParticles << std::endl;
        outfile << "# Pairs of analyzed Particles "  << numberOfEntries << std::endl;
        outfile << "# plot with (gnuplot): pl '" << t_strFileName << "' u 1:($2/$1**2) w histeps" << std::endl;
        outfile << "#_1 distance #_2 paircorrelation" << std::endl;
        outfile << *m_pHisto << std::endl;
        outfile.close();
        delete m_pHisto;
    };
private:
    unsigned long numberOfParticles;
    unsigned long numberOfEntries;
};

#endif // CALGOPAIRCORRELATION_H_INCLUDED
