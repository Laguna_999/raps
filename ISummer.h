#ifndef ISUMMER_H
#define ISUMMER_H


class ISummer
{
public:
    ISummer(double dBinSize):m_dBinSize(dBinSize) 
    {
        if(dBinSize < 0)
        {
            throw std::string ("negative BinSize is not supported in ISummer.");
        }
    };
    virtual ~ISummer()
    {
        m_mapHistogram.clear();
    };

    virtual void Add (double Key) = 0;
    virtual void Add (double Key, double value) = 0;

    double GetBinSize(){return m_dBinSize;};




    friend std::ostream& operator << (std::ostream &f, const ISummer& S)
    {
        if (!S.m_mapHistogram.empty())
        {
            std::cout << "saving histogram up to bin " << (--S.m_mapHistogram.end())->first << std::endl;
            for ( unsigned long i = 0; i < (--S.m_mapHistogram.end())->first; ++i)
            {
                f << static_cast<double>(S.m_dBinSize * i);
                f << "    ";
                if ( S.m_mapHistogram.count(i) )
                {
                    f << S.m_mapHistogram.find(i)->second;
                }
                else
                {
                    f << 0;
                }

                f << std::endl;
            }

        }
        return f;
    };

    // normalizes all Values to a max value of 1
    void NormalizePeak ()
    {
        // first: determine max value in Summer
        double t_dMax = 0;
        std::map<unsigned long,double>::const_iterator cit;
        for (cit = m_mapHistogram.begin(); cit != m_mapHistogram.end(); cit++)
        {
            if ( cit->second > t_dMax )
            {
                t_dMax = cit->second;
            }
        }


        std::map<unsigned long,double>::iterator it;
        for ( it = m_mapHistogram.begin(); it != m_mapHistogram.end(); ++it)
        {
            it->second /= t_dMax;
        }
        std::cout << m_mapHistogram.size() << " Values normalized with max of " << t_dMax << std::endl;

    };

    unsigned int GetSize()
    {
     return m_mapHistogram.size();
    } ;

    std::map<unsigned long, double>& GetMap() {return m_mapHistogram;};

    void clear ()
    {
        m_mapHistogram.clear();
    }


    void clean ()
    {

        std::map<unsigned long,double>::iterator it;
        for ( it = m_mapHistogram.begin(); it != m_mapHistogram.end(); ++it)
        {
            if( (*it).second <= 0)
            {
                m_mapHistogram.erase(it);
                it = m_mapHistogram.begin();
            }
        }
    }

protected:
    // this map stores pairs of double and int where the first int is the indexed square radius via m_dBinSize
    // and the latter int is the number of counted incidents
    std::map<unsigned long,double> m_mapHistogram;

    double m_dBinSize;

private:
};

#endif // ISUMMER_H
