// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.
#ifndef CFILELOADER_MPIDATA_H_INCLUDED
#define CFILELOADER_MPIDATA_H_INCLUDED

class cFileLoader_MPIData : public IFileLoader
{
    public:
    void LoadPositionsFromFile (std::string strFileName, std::vector<cPoint>& PositionList)
    {
        std::ifstream infile;
        infile.open(strFileName.c_str(),std::ifstream::in);
        if (infile.fail())
        {
            throw std::string ("Cannot load file " + strFileName);
        }

        std::string line;
        unsigned int linesloaded = 0;

        while (std::getline(infile, line))
        {
            std::istringstream iss(line);
            //std::cout << iss.str()<< std::endl;
            if(iss.str().compare(0,1,"#")==0)
            {
                continue;   // we have a comment

            }
            float x, y, z, al, ax, ay, az, bl, bx, by, bz, cl, cx, cy, cz;
            int label;
            if (!(iss >> label >> x >> y >> z >> al >> ax >> ay >> az >> bl >> bx >> by >> bz >> cl >> cx >> cy >> cz))
            {
                break;    // error
            }

            cPoint p;
            p.x = x;
            p.y = y;
            p.z = z;
            p.Alength = cl;
            p.Blength = bl;
            p.Clength = al;

            p.Aphi = atan2(ay,ax);
            p.Atheta = atan2(sqrt((ax*ax)+(ay*ay)),az);

            p.Bphi = atan2(by,bx);
            p.Btheta = atan2(sqrt((bx*bx)+(by*by)),bz);

            p.Cphi = atan2(cy,cx);
            p.Ctheta = atan2(sqrt((cx*cx)+(cy*cy)),cz);

            //std::cout << p.x << "\t" << p.y << "\t" << p.z << "\t" << p.Aphi << "\t" << p.Atheta << std::endl;

            linesloaded++;
            PositionList.push_back(p);

        }
        std::cout << "Lines loaded: " << linesloaded << std::endl << std::endl;
        infile.close();
    };
    std::string GetBoxInfoStrongFromFileName (std::string strFileName)
    {
        return "";

    };

    std::string FileTypeName()
    {
        return "MPI Data";

    };
};

#endif //CFILELOADER_MPIDATA_H_INCLUDED

