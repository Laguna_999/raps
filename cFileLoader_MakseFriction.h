// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CFILELOADER_MAKSEFRICTION_INCLUDED
#define CFILELOADER_MAKSEFRICTION_INCLUDED



#include "IFileLoader.h"

class cFileLoader_MakseFriction : public IFileLoader
{
    public:
    void LoadPositionsFromFile (std::string strFileName, std::vector<cPoint>& PositionList)
    {
        std::ifstream infile;
        infile.open(strFileName.c_str(),std::ifstream::in);
        if (infile.fail())
        {
            std::cout << "Cannot load file " << strFileName << std::endl;
            return;
        }

        std::string line;
        unsigned int linesloaded = 0;
        std::getline(infile, line);
        std::istringstream dimstream(line);
        float off[3], size[3];
        dimstream >> off[0] >> size[0];
        dimstream >> off[1] >> size[1];
        dimstream >> off[2] >> size[2];
        while (std::getline(infile, line))
        {
            std::istringstream iss(line);
            float x, y, z;
            if (!(iss >> x >> y >> z))
            {
                break;    // error
            }

            cPoint p;
            p.x = x;
            p.y = y;
            p.z = z;
            linesloaded++;
            PositionList.push_back(p);

        }
        std::cout << "Lines loaded: " << linesloaded << std::endl << std::endl;
        std::cout << "Dimensions: " << std::endl;
        std::cout << "X\t" << off[0] << "\t" << size[0] <<std::endl;
        std::cout << "Y\t" << off[1] << "\t" << size[1] <<std::endl;
        std::cout << "Z\t" << off[2] << "\t" << size[2] <<std::endl;

        //std::stringstream t_strstrBoxGeometry;
        //t_strstrBoxGeometry << "[" << off[0] << ":"<< size[0] << "] � [" << off[1] << ":" << size[1] << "] � [" << off[2] << ":" << size[2] << "]";
        //t_strBoxGeometryInformation = t_strstrBoxGeometry.str();

        infile.close();
    };

    std::string GetBoxInfoStrongFromFileName (std::string strFileName)
    {
        return "";

    };

    std::string FileTypeName()
    {
        return "Makse_Friction";

    };

};



#endif // CFILELOADER_MAKSEFRICTION_INCLUDED
