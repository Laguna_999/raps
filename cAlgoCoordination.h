// Copyright 2015 Simon Weis

// This file is part of RAPS.
//
// RAPS is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// RAPS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with RAPS.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CALGOCOORDINATION_H_INCLUDED
#define CALGOCOORDINATION_H_INCLUDED

#include "IAlgorithm.h"

class cAlgoCoordination :public IAlgorithm
{
public:
    void Calculate (std::vector<cPoint>& rPoints, ISummer* Histogram )
    {
        m_pHisto = new cSummerLinear(Histogram->GetBinSize());
        m_uiNumberOfPoints = rPoints.size();
        std::cout << "Creating Contact Number Histogram with CutOff Radius" << m_dCutOff << std::endl;
        float difX, difY, difZ, distanceSquared;
        for (   std::vector<cPoint>::const_iterator cit1 = rPoints.begin();
                cit1 != rPoints.end();
                ++cit1)
        {
            unsigned int coordination = 0;
            for (   std::vector<cPoint>::const_iterator cit2 = rPoints.begin();
                    cit2 != rPoints.end();
                    ++cit2)
            {
                if(cit1 != cit2)
                {
                    difX = cit1->x - cit2->x;
                    difY = cit1->y - cit2->y;
                    difZ = cit1->z - cit2->z;
                    distanceSquared = difX*difX + difY*difY + difZ*difZ;
                    float distance = sqrt(distanceSquared);
                    if (distance <= m_dCutOff)
                        coordination++;
                }
            }
            m_pHisto->Add(coordination);
        }


        // calculating mean Contact number
        m_dMeanContactNumber = 0.0f;
        std::map<unsigned long, double>::iterator it;
        for(    it = m_pHisto->GetMap().begin();
                it != m_pHisto->GetMap().end();
                ++it)
                {
                    m_dMeanContactNumber += static_cast<double>(it->first) * it->second;
                }
        m_dMeanContactNumber /= static_cast<double>( m_uiNumberOfPoints);
        std::cout << "mean contact number is " << m_dMeanContactNumber << std::endl;
    };

    ISummer* m_pHisto;

    double m_dCutOff;
    double m_dMeanContactNumber;
    unsigned int m_uiNumberOfPoints;

    virtual void Save (std::string parseInfo)
    {
        std::cout << "Writing Contact Number File" << std::endl;
        std::string t_strFileName = "co_" + parseInfo +".dat";
        std::ofstream outfile(t_strFileName.c_str());

        outfile << GetInfoString() << std::endl;
        outfile << "# Created on "  << GetCurrentDateTime() << std::endl;
        outfile << "# Histogram of Coordinations" << std::endl;
        outfile << "# Created on "  << GetCurrentDateTime() << std::endl;
        outfile << "# Bin Size "  << m_pHisto->GetBinSize() << std::endl;
        outfile << "# Mean Contact Number " << m_dMeanContactNumber << std::endl;

        outfile << "# Contact Number \t Occurrence" << std::endl;
        outfile << *m_pHisto << std::endl;
        outfile.close();
        delete m_pHisto;
    };

};

#endif // CALGOCOORDINATION_H_INCLUDED
